﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

	public string playerName;
	public int goldCount;
	public App.Items.Ship.Ship mainShip;

}
