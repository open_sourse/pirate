﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniApp.Items.Ship
{
	public class ShipInfo : MonoBehaviour {

		public Ship ship;
		Body body {get {return ship.body; }}
		Sail sail {get {return ship.sail; }}
		Rig rig {get {return ship.rig; }}
		Rigging rigging {get {return ship.rigging; }}

		List<Cannon> rightCannons_L {get {return ship.rightCannons_L; }}
		List<Cannon> leftCannons_L {get {return ship.leftCannons_L; }}

		public string shipName;

		public float maxSpeed {get {return (body.maxSpeed + sail.maxSpeed)/2; }}
		public float acceleration {get {return (body.acceleration + rig.acceleration + sail.acceleration)/3; } }
		public float rotationSpeed {get {return (body.rotationSpeed + rig.rotationSpeed + rigging.rotationSpeed)/3;} }


		public float hitPoint {get {return body.hitPoint + sail.hitPoint + rig.hitPoint + rigging.hitPoint; }}
		public float armorValue {get {return (body.armorValue + sail.armorValue + rigging.armorValue + rig.armorValue)/4; }}
		public float armorCount {get {return (body.armorCount + sail.armorCount + rigging.armorCount + rig.armorCount)/4; }}

		public int cost {get {return body.cost + sail.cost + rig.cost + rigging.cost + GetCost(); }}
		public float wearout { get {return (100*(body.currentWearout + sail.currentWearout + rig.currentWearout + rigging.currentWearout + GetMaxWearout())/
			(body.maxWearout + sail.maxWearout + rig.maxWearout + rigging.maxWearout + GetCurrentWearout())); }}

		public int maxCannons {get {return body.maxCannons; }}


		public int damage {get {return GetDamage (); }} 

		int GetDamage(){
			int i = 0;
			rightCannons_L.ForEach (c => i += (c.bodyDamage + c.rigDamage + c.riggingDamage + c.sailDamage)/4);
			leftCannons_L.ForEach (c => i += (c.bodyDamage + c.rigDamage + c.riggingDamage + c.sailDamage)/4);
			return i;
		}
		int GetCost(){
			int i = 0;
			rightCannons_L.ForEach (c => i += c.cost);
			leftCannons_L.ForEach (c => i += c.cost);
			return i;
		}
		float GetMaxWearout(){
			float i = 0;
			rightCannons_L.ForEach (c => i += c.maxWearout);
			leftCannons_L.ForEach (c => i += c.maxWearout);
			return i;
		}
		float GetCurrentWearout(){
			float i = 0;
			rightCannons_L.ForEach (c => i += c.currentWearout);
			leftCannons_L.ForEach (c => i += c.currentWearout);
			return i;
		}
	}
}