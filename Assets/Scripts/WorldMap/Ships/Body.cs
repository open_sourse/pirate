﻿using System.Collections;
using UniApp.Items;

namespace UniApp.Items.Ship
{
	public class Body : Item
	{
		public float maxSpeed;
		public float acceleration;
		public float rotationSpeed;

		public int maxCannons;
	}
}
