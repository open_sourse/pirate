﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UniApp.Items.Ship{
	[RequireComponent(typeof(UniApp.Pool.Shape))]
	//[RequireComponent(typeof(UniApp.Items.Ship.Controll))]

	[RequireComponent(typeof(ShipInfo))]
	//для отладки
	[RequireComponent(typeof(Body))]
	[RequireComponent(typeof(Sail))]
	[RequireComponent(typeof(Rig))]
	[RequireComponent(typeof(Rigging))]
	//
	public class Ship : MonoBehaviour {
		public Body body;
		public Sail sail;
		public Rig rig;
		public Rigging rigging;

		public ShipInfo shipInfo;
		public Controll shipControll;

		public List<Cannon> leftCannons_L;
		public List<Cannon> rightCannons_L;

		public List<UniApp.Items.Ship.Bullet> bullet_L = new List<UniApp.Items.Ship.Bullet>();

		public float currentSpeed = 0f;
		public bool moving = false;


	}
}
