﻿using System.Collections;

namespace UniApp.Items.Ship
{
	public class Sail : Item
	{
		public float maxSpeed;
		public float acceleration;
		bool g_raise;
		public bool raised 
		{
			get {return g_raise; } 
			set {g_raise = base.status == Status.WithoutWearout ? false : value; } 
		}
	}
}
