﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UniApp.Items.Ship{
	public class Controll : MonoBehaviour {
		public Ship ship;

		void Update()
		{
			Move (ship);
			Rotation (ship);
			Fire (ship);
		}

		public void Move(UniApp.Items.Ship.Ship _ship)
		{
			if (Input.GetKey (KeyCode.W)) {
				_ship.sail.raised = true;
			} else if (Input.GetKey (KeyCode.S)) {
				_ship.sail.raised = false;
			}

			if (_ship.currentSpeed > 1) {
				transform.Translate (transform.forward * _ship.currentSpeed * Time.deltaTime);
			}

			_ship.moving = _ship.currentSpeed > 10 ? true : false;

			_ship.currentSpeed = _ship.sail.raised == true ? 
				_ship.currentSpeed < _ship.shipInfo.maxSpeed ? _ship.currentSpeed + ((_ship.shipInfo.acceleration/60) ) : _ship.shipInfo.maxSpeed :
				_ship.currentSpeed > 0 ? _ship.currentSpeed - ((_ship.shipInfo.acceleration/60) ) : 0;
		}

		public void Fire(UniApp.Items.Ship.Ship _ship)
		{
			if (Input.GetKey (KeyCode.E)) {
				_ship.rightCannons_L.ForEach (c =>  c.Fire ());
			}
			if (Input.GetKey (KeyCode.Q)) {
				_ship.leftCannons_L.ForEach (c =>  c.Fire ());
			}
		}

		public void Rotation(UniApp.Items.Ship.Ship _ship)
		{
			if (!_ship.moving) return;

			if (Input.GetKey (KeyCode.A)) {
				transform.Rotate (new Vector3 (0, 0, (_ship.shipInfo.rotationSpeed * (1 + _ship.currentSpeed/_ship.shipInfo.maxSpeed)) * Time.deltaTime ));
			} 
			if (Input.GetKey (KeyCode.D)) {
				transform.Rotate (new Vector3 (0, 0, -(_ship.shipInfo.rotationSpeed * (1 + _ship.currentSpeed/_ship.shipInfo.maxSpeed)) * Time.deltaTime ));
			}
		}
	}
}