﻿using UnityEngine;
using System.Collections;
namespace UniApp.Items.Ship{
	[RequireComponent(typeof(UniApp.Pool.Shape))]
	public class Bullet : MonoBehaviour
	{
		public int bodyDamage;
		public int sailDamage;
		public int rigDamage;
		public int riggingDamage;

		public float currentSpeed = 0f;
		public float impulse = 1f;

		public float awakeTime;
		public float lifeTime = 3f;

		public Vector3 direction;

		public string creatorName;
		void Update(){

			this.transform.position = new Vector3(
				this.transform.position.x + direction.x * currentSpeed * Time.deltaTime, 
				0, 
				this.transform.position.z + direction.z * currentSpeed * Time.deltaTime);

			currentSpeed += impulse * Time.deltaTime;

			if (Time.time > awakeTime + lifeTime) {
				DestroyThis ();
			}
		}
		void OnTriggerEnter(Collider _col)
		{
			if (Time.time < awakeTime + 0.1f) return;
			var effects = _col.GetComponent<UniApp.General.Effects> ();
			if (effects == null) return;

			effects.damage (this);
			DestroyThis ();
		}
		void DestroyThis()
		{
			var shape = gameObject.GetComponent<UniApp.Pool.Shape> ();
			shape.objectPool.Push (shape);
		}
	}
}
