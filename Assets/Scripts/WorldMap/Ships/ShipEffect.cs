﻿using UnityEngine;
using System.Collections;
namespace UniApp.Items.Ship{
	[RequireComponent(typeof(UniApp.General.Effects))]
	public class ShipEffect : MonoBehaviour
	{
		public Ship ship;
		public UniApp.General.Effects effects;

		void Awake(){
			effects.damage = new UniApp.General.Effects.Damage (Damage);
		}

		public void Damage(Bullet _bullet)
		{
			HpDamage (_bullet);
			WearoutDamage (_bullet);
			Dying ();
		}
		void HpDamage(Bullet _bullet)
		{
			DamageDistribution (ship.body, _bullet.bodyDamage);
			DamageDistribution (ship.sail, _bullet.sailDamage);
			DamageDistribution (ship.rig, _bullet.rigDamage);
			DamageDistribution (ship.rigging, _bullet.riggingDamage);
		}
		void WearoutDamage(Bullet _bullet){
			WearoutDistribution (ship.body, _bullet.bodyDamage);
			WearoutDistribution (ship.sail, _bullet.sailDamage);
			WearoutDistribution (ship.rig, _bullet.rigDamage);
			WearoutDistribution (ship.rigging, _bullet.riggingDamage);
		}
		void DamageDistribution(Item _damagingItem, float _incommingDamage){
			if (_damagingItem.armorCount > 0) {
				_damagingItem.hitPoint -= GetDamageValue(_incommingDamage, _damagingItem.armorValue, _damagingItem.armorCount);
				_damagingItem.armorCount -= GetDamageValue(_incommingDamage, _damagingItem.armorValue);
			} else {
				_damagingItem.hitPoint -= GetDamageValue(_incommingDamage, _damagingItem.armorValue);
			}

			if (_damagingItem.hitPoint <= 0) {
				_damagingItem.status = Item.Status.WithoutHp;
			}
		}
		void WearoutDistribution(Item _damagingItem, float _incommingDamage)
		{
			if (_damagingItem.hitPoint > 0) {
				_damagingItem.currentWearout -= GetDamageValue(_incommingDamage, _damagingItem.armorValue);
			} else {
				_damagingItem.currentWearout -= GetDamageValue(_incommingDamage * 2, _damagingItem.armorValue);
			}

			if (_damagingItem.currentWearout <= 0) {
				_damagingItem.status = Item.Status.WithoutWearout;
			}
		}
		float GetDamageValue(float _incommingDamage, float _armorValue, float _armorCount = 0){
			float damage = _incommingDamage - _armorValue - _armorCount;
			return damage > 0 ? damage : 0;
		}
		void Dying()
		{
			if (ship.shipInfo.hitPoint <= 0) {
				var shape = ship.gameObject.GetComponent<UniApp.Pool.Shape> ();
				shape.objectPool.Push (shape);
			}
		}
	}
}
