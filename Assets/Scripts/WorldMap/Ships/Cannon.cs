﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace UniApp.Items.Ship{
	[RequireComponent(typeof(UniApp.Pool.Shape))]
	public class Cannon : Item
	{
		public string ownerName;

		[Range(1f, 999f)]
		public float fireSpeed;

		public int bodyDamage;
		public int sailDamage;
		public int rigDamage;
		public int riggingDamage;

		public float power = 10f;

		public bool charged = true;
		public float lastFire;

		public bool Loaded(out Bullet _bullet)
		{
			if (!charged) {
				_bullet = null;
				return false;
			}

			this.charged = false;
			_bullet = GetComponent<UniApp.Pool.Shape>().objectPool.Pop("bullet").GetComponent<Bullet>();
			return true;
		}

		public void Fire()
		{
			UniApp.Items.Ship.Bullet bullet;
			if (!Loaded (out bullet)) return; 
			FireProcess (bullet);

			StartCoroutine (Recharging ());
		}
		void FireProcess(UniApp.Items.Ship.Bullet _bullet){
			var shape = gameObject.GetComponent<UniApp.Pool.Shape>();
		
			_bullet.transform.position = this.transform.position;
			_bullet.GetComponent<UniApp.Pool.Shape>().objectPool = shape.objectPool;

			_bullet.bodyDamage = this.bodyDamage;
			_bullet.sailDamage = this.sailDamage;
			_bullet.rigDamage = this.rigDamage;
			_bullet.riggingDamage = this.riggingDamage;
			_bullet.creatorName = ownerName;

			_bullet.impulse = this.power;
			_bullet.currentSpeed = this.power*10;//это начальный импульс
			_bullet.awakeTime = Time.time;

			_bullet.direction = transform.right;

			lastFire = Time.time;
		}

		IEnumerator Recharging()
		{
			while (Time.time < lastFire + fireSpeed) {
				yield return new WaitForSeconds (0.1f);
			} 
				charged = true;
				yield break;
		}
	}
}
