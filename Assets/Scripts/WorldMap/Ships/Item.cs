﻿using System.Collections;
using UnityEngine;

namespace UniApp.Items
{
	public class Item : MonoBehaviour
	{
		public int cost;

		public float hitPoint;
		public float currentWearout;
		public float maxWearout;

		public float armorValue;
		public float armorCount;
		public Status status = Status.Fine;

		public enum Status 
		{
			Fine,
			WithoutHp,
			WithoutWearout
		}
	}
}
